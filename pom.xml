<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <!--
  Good tuturials that include full maven pom files.
  https://github.com/eugenp/tutorials
  -->
  <groupId>com.hk</groupId>
  <artifactId>hk-leetcode</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>hk-leetcode</name>
  <description>HK leetcode project</description>
  <url>http://maven.apache.org</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <java.version>1.8</java.version>
    <maven-compiler-plugin.version>3.7.0</maven-compiler-plugin.version>
    <!-- http://maven.apache.org/surefire/maven-surefire-plugin/dependency-info.html -->
    <maven-surefire-plugin.version>2.21.0</maven-surefire-plugin.version>

    <!-- https://mvnrepository.com/artifact/javax.validation/validation-api -->
    <validation-api.version>2.0.1.Final</validation-api.version>
    <!--
    https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine
    sample maven setup by junit team:
        https://github.com/junit-team/junit5-samples/blob/r5.1.1/junit5-maven-consumer/pom.xml
    -->
    <junit.jupiter.version>5.1.1</junit.jupiter.version>
    <junit.vintage.version>5.1.1</junit.vintage.version>
    <junit.platform.version>1.1.1</junit.platform.version>

    <!-- https://mvnrepository.com/artifact/org.testng/testng -->
    <testng.version>6.14.3</testng.version>
    <!-- https://mvnrepository.com/artifact/org.assertj/assertj-core -->
    <assertj-core.version>3.10.0</assertj-core.version>
    <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-collections4 -->
    <commons-collections4>4.1</commons-collections4>
    <!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
    <commons-io.version>2.6</commons-io.version>
    <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
    <commons-lang3.version>3.7</commons-lang3.version>
    <!-- http://logging.apache.org/log4j/2.x/maven-artifacts.html -->
    <log4j-core.version>2.11.0</log4j-core.version>
    <!-- https://mvnrepository.com/artifact/io.vavr/vavr -->
    <vavr.version>0.9.2</vavr.version>
    <!-- https://mvnrepository.com/artifact/org.immutables/value -->
    <immutables.version>2.6.1</immutables.version>
    <!-- https://mvnrepository.com/artifact/com.google.guava/guava -->
    <quava.version>25.1-jre</quava.version>
    <!-- https://mvnrepository.com/artifact/com.google.code.findbugs/jsr305 -->
    <jsr305.version>3.0.2</jsr305.version>
    <!-- https://mvnrepository.com/artifact/org.eclipse.collections/eclipse-collections -->
    <!-- https://www.eclipse.org/collections/ -->
    <!--  Practice with kata:  https://github.com/eclipse/eclipse-collections-kata -->
    <eclipse-collections.version>9.2.0</eclipse-collections.version>
  </properties>

  <dependencies>

    <dependency>
      <groupId>javax.validation</groupId>
      <artifactId>validation-api</artifactId>
      <version>${validation-api.version}</version>
    </dependency>


    <!-- junit 4:  https://mvnrepository.com/artifact/junit/junit
                   https://github.com/junit-team/junit4/wiki/Download-and-Install
    -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
    </dependency>



    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <version>${junit.jupiter.version}</version>
      <scope>compile</scope>
    </dependency>
    <!-- http://www.baeldung.com/junit-5  says engine is the only item required. -->
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <version>${junit.jupiter.version}</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-params</artifactId>
      <version>${junit.jupiter.version}</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.vintage</groupId>
      <artifactId>junit-vintage-engine</artifactId>
      <version>${junit.vintage.version}</version>
      <scope>compile</scope>
    </dependency>
    <!-- junit 5: Only required to run tests in an IDE that bundles an older version -->
    <dependency>
      <groupId>org.junit.platform</groupId>
      <artifactId>junit-platform-launcher</artifactId>
      <version>${junit.platform.version}</version>
      <scope>compile</scope>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.junit.platform/junit-platform-surefire-provider -->
    <dependency>
      <groupId>org.junit.platform</groupId>
      <artifactId>junit-platform-surefire-provider</artifactId>
      <version>${junit.platform.version}</version>
      <scope>compile</scope>
    </dependency>


    <dependency>
      <groupId>org.testng</groupId>
      <artifactId>testng</artifactId>
      <version>${testng.version}</version>
      <scope>compile</scope>
    </dependency>

    <dependency>
      <groupId>org.assertj</groupId>
      <artifactId>assertj-core</artifactId>
      <version>${assertj-core.version}</version>
      <scope>compile</scope>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-collections4</artifactId>
      <version>${commons-collections4}</version>
    </dependency>
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>${commons-io.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>${commons-lang3.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
      <version>${log4j-core.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <version>${log4j-core.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <version>${log4j-core.version}</version>
      <type>test-jar</type>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>io.vavr</groupId>
      <artifactId>vavr</artifactId>
      <version>${vavr.version}</version>
    </dependency>

    <dependency>
        <groupId>org.immutables</groupId>
        <artifactId>value</artifactId>
         <version>${immutables.version}</version>
    </dependency>

    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
      <version>${quava.version}</version>
    </dependency>

    <dependency>
      <groupId>com.google.code.findbugs</groupId>
      <artifactId>jsr305</artifactId>
      <version>${jsr305.version}</version>
    </dependency>

    <dependency>
      <groupId>org.eclipse.collections</groupId>
      <artifactId>eclipse-collections-api</artifactId>
      <version>${eclipse-collections.version}</version>
    </dependency>

    <dependency>
      <groupId>org.eclipse.collections</groupId>
      <artifactId>eclipse-collections</artifactId>
      <version>${eclipse-collections.version}</version>
    </dependency>

  </dependencies>

  <build>
    <plugins>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <!-- https://maven.apache.org/plugins/maven-compiler-plugin/dependency-info.html -->
        <version>${maven-compiler-plugin.version}</version>
        <configuration>
          <source>${java.version}</source>
          <target>${java.version}</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${maven-surefire-plugin.version}</version>
        <configuration>
          <testSourceDirectory>${basedir}/src/main/java/</testSourceDirectory>
          <testClassesDirectory>${project.build.directory}/classes/</testClassesDirectory>
          <includes>
            <include>**/*Test.java</include>
            <include>**/*TestCase.java</include>
          </includes>
          <properties>
            <!-- <includeTags>fast</includeTags> -->
            <excludeTags>slow</excludeTags>
            <!--
            <configurationParameters>
                junit.jupiter.conditions.deactivate = *
            </configurationParameters>
            -->
          </properties>
        </configuration>
        <dependencies>
          <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-surefire-provider</artifactId>
            <version>${junit.platform.version}</version>
          </dependency>
          <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit.jupiter.version}</version>
          </dependency>
        </dependencies>
      </plugin>

    </plugins>
  </build>

</project>
