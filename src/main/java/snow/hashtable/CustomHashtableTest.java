package snow.hashtable;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertFalse;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * on 8/30/18
 */
public class CustomHashtableTest {

    @BeforeClass
    public void beforeClass() {
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    @Test
    public void testPutSize() {
        CustomHashtable<String,String> under = new CustomHashtable<>();
        assertEquals(under.size(), 0);
        under.put("b", "air");
        assertEquals(under.size(), 1);

        // size 1, not 2; entry value got replaced since it's same key.
        under.put("b", "water");
        assertEquals(under.size(), 1);

        under.put("z", "zebra");
        assertEquals(under.size(), 2);

        for (int i = 0; i < 30; i++) {
            under.put(String.valueOf(i),  String.valueOf(i));
        }

        assertEquals(under.size(), 32);
    }

    @Test
    public void testGetRemove_String() {
        CustomHashtable<String,String> under = new CustomHashtable<>(3);
        // String objects
        assertNull(under.get("b"));

        under.put("b", "air");

        assertEquals(under.get("b"), "air");

        // put:  "same key"
        //   1. old value got replaced with new value.
        //   2. return value is the old value of same key.
        String oldValue = under.put("b", "water");
        assertEquals(oldValue, "air");
        assertEquals(under.get("b"), "water");
        //

        // Validate the removal of String key returns the removed value.
        assertEquals(under.remove("b"), "water");

        // Validate the removal of same key now returns null.
        assertNull(under.remove("b"));

        // Test containsKey of removed key.
        assertFalse(under.containsKey("absent"));
    }

    @Test
    public void testSize_Integer_ManyCollisions() {
        // Key: Integer;   Value: String
        CustomHashtable<Integer,String> under = new CustomHashtable<>(10);

        // Put
        for (int i = 0; i < 30; i++) {
            under.put(i,  String.valueOf(i) + "_string");
        }

        assertEquals(under.size(), 30);

        // get
        for (int i = 0; i < 30; i++) {
            assertEquals(under.get(i), String.valueOf(i)+"_string");
            assertTrue(under.containsKey(i));
        }

        // Remove
        for (int i = 0; i < 30; i++) {
            assertEquals(under.remove(i), String.valueOf(i)+"_string");
            assertFalse(under.containsKey(i));
        }

        assertEquals(under.size(), 0);
    }

}
