package snow.hashtable;

import javax.validation.constraints.NotNull;

public interface CustomHashtableInterface<K,V> {
    V put(@NotNull K key, @NotNull V value);

    V get(@NotNull K key);

    boolean containsKey(@NotNull K key);

    V remove(@NotNull K key);

    int size();
}
