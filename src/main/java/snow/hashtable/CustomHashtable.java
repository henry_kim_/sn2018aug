package snow.hashtable;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.ListIterator;
import javax.validation.constraints.NotNull;

/**
 Custom Implementation of hash table without using any API.

 Hint: use ArrayList/LinkedList to implement.

 Submit your solution as zip file or create a GIT project and share the link.
 Add unit test to test your solution

 *  on 8/30/18
 */
public class CustomHashtable<K,V> implements CustomHashtableInterface<K,V> {

    private static class HashEntry<K,V>  {
        final K key;
        V value;

        HashEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // Hashtable : we'll use an array of buckets.
    // Each bucket is a LinkedList, per the hint.

    private transient LinkedList<HashEntry<K,V>>[] buckets;
    private transient int size;

    // Intentional small table size, so we can see the collisions and the linked list.
    private static final int DEFAULT_TABLE_SIZE = 10;

    public CustomHashtable() {
        this(DEFAULT_TABLE_SIZE);
    }

    @SuppressWarnings("unchecked")
    public CustomHashtable(int capacity) {
        buckets = new LinkedList[capacity];
    }

    /**
     * Put the key/value pair in hash table.
     * Don't allow null key or value, to follow java's behavior of hash table.
     * @param key key of entry
     * @param value value of entry
     * @return old value if the key exists; otherwise null.
     */
    @Override
    public synchronized V put(@NotNull K key, @NotNull V value) {
        int hash = getHash(key);
        Hashtable x;

        // check buckets table for a linked list.
        LinkedList<HashEntry<K,V>> list = buckets[hash];
        if (list == null) {
            // store in linkedList and return.
            buckets[hash] = new LinkedList<>();
            buckets[hash].add(new HashEntry<>(key, value));
            size++;
            return null;
        }

        // Iterate the chain of entries, and if we find the key, replace the value and return.
        // If not found during loop, add entry to linked list.
        for (HashEntry<K,V> entry : list) {
            if (entry.key.equals(key)) {
                // If found the key, replace the value, and return.
                V old = entry.value;
                entry.value = value;
                return old;
            }
        }
        list.add(new HashEntry<>(key, value));
        size++;
        return null;
    }

    @Override
    public synchronized V get(@NotNull K key) {
        int hash = getHash(key);
        LinkedList<HashEntry<K,V>> list = buckets[hash];
        if (list == null) {
            return null;
        }
        for (HashEntry<K,V> entry : list) {
            if (entry.key.equals(key)) {
                return entry.value;
            }
        }
        return null;
    }

    @Override
    public synchronized boolean containsKey(@NotNull K key) {
        return get(key) != null;
    }

    @Override
    public synchronized V remove(@NotNull K key) {
        int hash = getHash(key);
        LinkedList<HashEntry<K,V>> list = buckets[hash];
        if (list == null) {
            return null;
        }

        // It's optimal to use ListIterator to find the element, then remove(),
        // instead of using a foreach and then remove(integer).
        ListIterator<HashEntry<K,V>> iter = list.listIterator();
        while (iter.hasNext()) {
            HashEntry<K,V> entry = iter.next();
            if (key.equals(entry.key)) {
                size--;
                iter.remove();
                return entry.value;
            }
        }
        return null;
    }

    @Override
    public synchronized int size() {
        return size;
    }

    // ========================================================
    // private methods
    // ========================================================

    private int getHash(@NotNull Object key) {
        return key.hashCode() % buckets.length;
    }

}
