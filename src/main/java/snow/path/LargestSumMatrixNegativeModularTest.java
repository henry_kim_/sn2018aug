package snow.path;

import org.testng.annotations.BeforeClass;

/**
 * on 8/30/18
 */
public class LargestSumMatrixNegativeModularTest extends LargestSumTestCommon {
    @BeforeClass
    public void setup() {
        underTest = new LargestSumMatrixNegativeModular();
        System.out.println("class under test: " + underTest.getClass().getSimpleName());
    }
}
