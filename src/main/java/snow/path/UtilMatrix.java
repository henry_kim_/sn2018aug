package snow.path;

import java.util.List;

public class UtilMatrix {

	public static void printOccupyMatrixFromPath(List<Point> path, int rows, int cols) {
		int[][] matrix = fromPoinstListToMatrix(path, rows, cols);
		printMatrixFormatWidth(matrix, determineMatrixCellWidth(matrix));
	}

	public static int[][] fromPoinstListToMatrix(List<Point> path, int rows, int cols) {
		int[][] matrix = new int[rows][cols];
		int count = 0;
		for (Point p : path) {
			matrix[p.row][p.column] = ++count;
		}
		return matrix;
	}

	public static void printMatrixFromPath(int[][] matrix, List<Point> path) {
		if (path == null)
			return;
		int count = 0;
		System.out.println("Points: ");
		for (Point p : path) {
			if (count > 0) {
				System.out.print(", ");
			}
			System.out.print("("+p.row + "," + p.column+")");
			count++;
		}
		System.out.println();
		System.out.println("Values path: ");
		count = 0;
		for (Point p : path) {
			if (count > 0) {
				System.out.print(", ");
			}
			System.out.print(matrix[p.row][p.column]);
			count++;
		}
		System.out.println();
	}

	public static void printMatrix(int[][] matrix) {
		printMatrixFormatWidth(matrix, determineMatrixCellWidth(matrix));
	}

	public static int determineMatrixCellWidth(int[][] matrix) {
		int width = 3;
		for (int[] aMatrix : matrix) {
			for (int anAMatrix : aMatrix) {
				int abs = Math.abs(anAMatrix);
				if (width < 6 && abs >= Math.pow(10, 3)) {
					width = 8;
				} else if (width < 4 && abs >= Math.pow(10, 2)) {
					width = 7;
				}
			}
		}
		return width;
	}

	public static void printMatrix(boolean[][] matrix) {
		for (boolean[] aMatrix : matrix) {
			for (boolean anAMatrix : aMatrix) {
				if (anAMatrix) {
					System.out.print(" 1");
				} else {
					System.out.print(" 0");
				}
			}
			System.out.println();
		}
	}

	public static void printMatrixFormatWidth(int[][] matrix, int width) {
		printMatrixFormat(matrix, "%" + width + "d");
	}

	public static void printMatrixFormat(int[][] matrix, String format) {
		for (int[] row : matrix) {
			for (int col : row) {
				System.out.printf(format, col);
			}
			System.out.println();
		}
		System.out.println();
	}

}
