package snow.path;

public interface LargestSumInterface {
    int findMaxSum(int[][] matrix);
}
