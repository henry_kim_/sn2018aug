package snow.path;

/**
 *  on 10/13/15.
 */
public class Point {
    public final int row, column;
    final public int x, y;
    public Point(int row, int column) {
        this.row = row;
        this.column = column;
        this.x = row;
        this.y = column;
    }

    @Override
    public int hashCode() {
        return row * 3000 + column;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        } else {
            Point p = (Point) obj;
            return this.row == p.row && this.column == p.column;
        }
    }
    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }
}
