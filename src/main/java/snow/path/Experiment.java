package snow.path;

import java.util.ArrayList;
import java.util.List;

public class Experiment {

    public static void main(String[] args) {
        List<Point> path = new ArrayList<>();
        path.add(new Point(0, 0));
        path.add(new Point(0, 1));
        path.add(new Point(1, 0));

        int[][] matrix = new int[][]{
                {1, 2, 3},
                {4, -3, 6},
                {7, 8, 9}
        };

        int sum = 0;
        for (Point p : path) {
            sum += matrix[p.row][p.column];
        }
        System.out.println(sum);

        sum = path.stream().mapToInt((point) -> matrix[point.row][point.column]).sum();

        System.out.println(sum);
    }
}
