package snow.path;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * //////////////////////////////////////////////////////////
 *  <<    ...See this class, which is the preferred design...   instead of
 *                                  {@link LargestSumMatrixNegativeModular}
 *
 * Combined design  (Recursive method does it all: find all paths, sum.)
 *
 *     a) Determine "all paths" from start to finish cell, and iterate each pre-computed path.
 *
 *     b) Use each "pre-computed" path to sum the cells.
 *
 *     c) Update max sum.
 *
 * //////////////////////////////////////////////////////////

 *
 * Find max sum from start to end of matrix, which can include negative numbers.
 * Move includes diagonals. start is 0,0; end is bottom right.
 *
 * Because there are negatives, and can move up and left, need to examine every possible path.
 *
 * on 8/29/18 created by Henry Kim
 */
public class LargestSumMatrixNegativeCombined implements LargestSumInterface {

    static boolean debug = false;

    static final int[][] DIRECTIONS = {
            // {row,col}
            // right, left, down, up.
            {0, 1}, {0, -1}, {1, 0}, {-1, 0},
            // diagonals.  down-right, down-left, up-right, up-left
            {1, 1}, {1, -1}, {-1, 1}, {-1, -1}
    };

    @Override
    public int findMaxSum(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }

        int[][] maze = buildOnesMatrix(matrix);
        List<Point> onePath = new ArrayList<>();
        Map<Integer, Boolean> matrixOccupyMap = new HashMap<>();
        int[] maxResult = new int[] { Integer.MIN_VALUE };

        buildPaths(0, 0, maze, onePath, matrixOccupyMap, matrix, maxResult);

        return maxResult[0];
    }

    /**
     * Recursively build a list of all paths. It's plotting a strict maze.
     * Then, for each path in list, sum the path.
     *
     * 1 means open.  0 means blocked.
     */
    private void buildPaths(int row, int col, int[][] maze,
            List<Point> path,
            Map<Integer, Boolean> matrixOccupyMap,
            int[][] matrix,
            int[] maxResult)
    {
        int dest_x = maze[0].length - 1, dest_y = maze.length - 1;
        if (col < 0 || row < 0 || col >= maze[0].length || row >= maze.length
                || maze[row][col] == 0) {
            return;
        }
        Point p = new Point(row, col);
        path.add(p);
        if (row == dest_y && col == dest_x) {
            // Reached end cell.
            // Dynamic Programming:
            //    Using hash map, decide whether this is a duplicate set of matrix cell occupation.
            int hashOfMatrix = hashOfMatrix(maze[0].length, path);

            if (!matrixOccupyMap.containsKey(hashOfMatrix)) {
                matrixOccupyMap.put(hashOfMatrix, Boolean.TRUE);

                // 2nd design: SUM directly here within the loop, and update max.
                //
                int sum = path.stream().mapToInt((point) -> matrix[point.row][point.column]).sum();
                if (sum > maxResult[0]) {
                    maxResult[0] = sum;
                }
            }
        } else {
            // Block current cell
            maze[row][col] = 0;

            // DFS recursion into each of the 8 directions.
            //
            for (int[] direction : DIRECTIONS) {
                buildPaths(row + direction[0], col + direction[1], maze,
                        path, matrixOccupyMap, matrix, maxResult);
            }

            // Open the cell again
            // When the entire matrix has been processed, the maze will be back to all 1's.
            maze[row][col] = 1;
        }

        // Remove the last path item, to restore the state at beginning of method.
        path.remove(path.size()-1);
    }

    /**
     * Build a matrix of 1's.
     */
    private int[][] buildOnesMatrix(int[][] matrix) {
        int[][] onesMatrix = new int[matrix.length][matrix[0].length];
        Arrays.stream(onesMatrix).forEach(row -> Arrays.fill(row, 1));
        return onesMatrix;
    }

    private int hashOfMatrix(int numColumns, List<Point> path) {
        int hash = 0;
        for (Point p : path) {
            hash += (p.row * numColumns * 10) + p.column + 1;
        }
        return hash;
    }

}
