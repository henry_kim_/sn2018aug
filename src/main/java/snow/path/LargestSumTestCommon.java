package snow.path;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * 8/30/18
 * For reference, there are 215 diff paths for a 3 x 3 matrix.
 */
public class LargestSumTestCommon {
    LargestSumInterface underTest;

    @BeforeClass
    public void setup() {
    }

    @Test
    public void testProvided() {
        int[][] matrix = new int[][]{
                {4, 8, 100, -1000},
                {70, -10, 2000, 70},
                {-5, -21, -6, 8},
                {10000, -20, 15, 21}
        };
        validate(matrix, 12244);
    }

    @Test
    public void testMiddleNegative() {
        int[][] matrix = new int[][]{
                {1, 2, 3},
                {4, -3, 6},
                {7, 8, 9}
        };
        validate(matrix, 37);
    }

    @Test
    public void testAllOnes() {
        int[][] matrix = new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        validate(matrix, 45);
    }

    @Test
    public void testAllOnes2x3() {
        int[][] matrix = new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        };
        validate(matrix, 21);
    }

    @Test
    public void testAllOnes2x3Negative() {
        int[][] matrix = new int[][]{
                {1, -2, 3},
                {4, 5, 6}
        };
        validate(matrix, 19);
    }


    // ------------------------------------------------
    // helpers
    // ------------------------------------------------

    private void validate(int[][] matrix, int expectedMax) {
        if (underTest == null) {
            throw new RuntimeException("Run the subclass instead of this class");
        }
        int max = underTest.findMaxSum(matrix);

        System.out.println("\nmax: " + max);
        UtilMatrix.printMatrix(matrix);

        assertEquals(max, expectedMax);
    }


}
