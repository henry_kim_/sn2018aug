package snow.path;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * //////////////////////////////////////////////////////////
 *
 * See preferred design:  {@link LargestSumMatrixNegativeCombined}
 *
 * Modular design    (The recursive method only builds the possible paths, and not the sums.)
 *
 *     a) Generate the list of "all paths" from start to finish cell.
 *
 *     b) Use each "pre-computed" path to sum the cells.
 *
 *     c) Update max sum.
 *
 * //////////////////////////////////////////////////////////

 *
 * Find max sum from start to end of matrix, which can include negative numbers.
 * Move includes diagonals. start is 0,0; end is bottom right.
 *
 * Because there are negatives, and can move up and left, need to examine every possible path.
 *
 * on 8/29/18 created by Henry Kim
 */
public class LargestSumMatrixNegativeModular implements LargestSumInterface {

    static boolean debug = false;

    static final int[][] DIRECTIONS = {
            // {row,col}
            // right, left, down, up.
            {0, 1}, {0, -1}, {1, 0}, {-1, 0},
            // diagonals.  down-right, down-left, up-right, up-left
            {1, 1}, {1, -1}, {-1, 1}, {-1, -1}
    };

    @Override
    public int findMaxSum(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }
        int numRows = matrix.length;
        int numCols = matrix[0].length;

        //
        // Generate the list of "all paths" from start to finish cell.
        //
        int[][] maze = buildOnesMatrix(matrix);
        List<List<Point>> listOfPaths = new ArrayList<>();
        List<Point> onePath = new ArrayList<>();
        Map<Integer, Boolean> matrixOccupyMap = new HashMap<>();
        // Recursive method
        buildPaths(0, 0, maze, listOfPaths, onePath, matrixOccupyMap);

        //
        // Iterate each path, and find the sum using the path, and update max.
        //
        int max = Integer.MIN_VALUE;
        List<Point> maxPath = null;
        for (List<Point> path : listOfPaths) {
            if (debug) {
                System.out.println("\n ways to occupy: " + ". Path: " + path);
                UtilMatrix.printOccupyMatrixFromPath(path, numRows, numCols);
            }

            int sum = path.stream().mapToInt((point) -> matrix[point.row][point.column]).sum();
            if (sum > max) {
                max = sum;
                maxPath = new ArrayList<>(path);
            }
        }
        UtilMatrix.printMatrixFromPath(matrix, maxPath);
        return max;
    }

    /**
     * Recursively build a list of all paths. It's plotting a strict maze.
     *
     * 1 means open.  0 means blocked.
     */
    private void buildPaths(int row, int col, int[][] maze,
            List<List<Point>> listOfPaths,
            List<Point> path,
            Map<Integer, Boolean> matrixOccupyMap) {
        int dest_x = maze[0].length - 1, dest_y = maze.length - 1;
        if (col < 0 || row < 0 || col >= maze[0].length || row >= maze.length
                || maze[row][col] == 0) {
            return;
        }
        Point p = new Point(row, col);
        path.add(p);
        if (row == dest_y && col == dest_x) {
            // Reached end cell.
            // Dynamic Programming:
            //    Using hash map, decide whether this is a duplicate set of matrix cell occupation.
            int hashOfMatrix = hashOfMatrix(maze[0].length, path);

            if (!matrixOccupyMap.containsKey(hashOfMatrix)) {
                matrixOccupyMap.put(hashOfMatrix, Boolean.TRUE);
                // Copy current path to the list of all paths.
                listOfPaths.add( new ArrayList<>(path) );
            }
        } else {
            // Block current cell
            maze[row][col] = 0;

            // DFS recursion into each of the 8 directions.
            //
            for (int[] direction : DIRECTIONS) {
                buildPaths(row + direction[0], col + direction[1], maze,
                        listOfPaths, path, matrixOccupyMap);
            }

            // Open the cell again.
            // When the entire matrix has been processed, the maze will be back to all 1's.
            maze[row][col] = 1;
        }

        // Remove the last path item.
        path.remove(path.size()-1);
    }

    /**
     * Build a matrix of 1's.
     */
    private int[][] buildOnesMatrix(int[][] matrix) {
        int[][] onesMatrix = new int[matrix.length][matrix[0].length];
        Arrays.stream(onesMatrix).forEach(row -> Arrays.fill(row, 1));
        return onesMatrix;
    }

    private int hashOfMatrix(int numColumns, List<Point> path) {
        int hash = 0;
        for (Point p : path) {
            hash += (p.row * numColumns * 10) + p.column + 1;
        }
        return hash;
    }

}
